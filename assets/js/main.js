var langButton = document.querySelector(".languages");
langButton.addEventListener("click", toggleLang);

var contactBtn = document.querySelector(".contact");
contactBtn.addEventListener("click", toggleContact);

var contactCloseBtn = document.querySelector(".contactClose");
contactCloseBtn.addEventListener("click", toggleContact);

var contactForm = document.querySelector(".contactForm form");
var lang = contactForm.parentNode.getAttribute("data-lang");
var sending = false;

var text = {
    "en":[
        "Sending Email...",
        "Success!",
        "Sending Failed."
    ],
    "de":[
        "Email wird gesendet...",
        "Erfolg!",
        "Sendung fehlgeschlagen."
    ],
    "cn":[
        "发送邮件...",
        "成功!",
        "发送失败."
    ],
    "it":[
        "Inviare e-mail...",
        "Successo!",
        "Invio fallito."
    ]
}

function handleFormSubmit(){
    if(!sending){
        sending = true;
        contactForm.querySelector(".formMessage").innerHTML = text[lang][0];

        var name = contactForm.querySelector("input[name='name']");
        var email = contactForm.querySelector("input[name='email']");
        var msg = contactForm.querySelector("textarea[name='msg']");

        sendMail(name.value, email.value, msg.value);
    }
    return false;
}
function sendMail(name, email, msg) {
    var xhttp;
    if (window.XMLHttpRequest) {
        // code for modern browsers
        xhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            sending = false;

            if(this.response == "Success"){
                contactForm.reset();
                contactForm.querySelector(".formMessage").innerHTML = text[lang][1]
            }else{
                contactForm.querySelector(".formMessage").innerHTML = text[lang][2];
            }
        }
    };
    xhttp.open("POST", "sendMail.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("name="+name+"&email="+email+"&msg="+msg);
}

function toggleContact(){
    var obj = document.querySelector(".contactWrapper");
    contactForm.querySelector(".formMessage").innerHTML = "";

    if(obj.style.maxHeight == "0px" || obj.style.maxHeight == ""){
        obj.style.maxHeight = "1000px";
    }else {
        obj.style.maxHeight = "0px";
    }
}
function toggleLang(){
    var obj = document.querySelector(".languages ul");

    if(obj.style.display == "none" || obj.style.display == ""){
        obj.style.display = "block";
    }else {
        obj.style.display = "none";
    }
}
