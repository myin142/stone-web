<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../vendor/autoload.php';

if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['msg'])){

    // Get SMTP Account Informations
    $file = fopen("../smtpAccount.txt","r");
    $smtpEmail = fgets($file);
    $smtpPass = fgets($file);
    fclose($file);

    // Mail Details
    $name = htmlspecialchars($_POST['name']);
    $email_from = htmlspecialchars($_POST['email']);
    $msg = htmlspecialchars($_POST['msg']);
    $email_to = file_get_contents("../email.txt");
    $subject = "Message from ".$name;

    $mail = new PHPMailer(true);
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // 0: off, 1: client msg, 2: client + server msg
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com:587';                   // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $smtpEmail;                         // SMTP username
        $mail->Password = $smtpPass;                          // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($email_from, "Mail Server");
        $mail->addAddress($email_to);
        $mail->addReplyTo($email_from, $name);

        //Content
        $mail->isHTML(false);
        $mail->Subject = $subject;
        $mail->Body = $msg;

        $mail->send();
        echo 'Success';
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}

?>
