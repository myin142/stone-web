<?php

/***************
INITIALIZATIONS
***************/

// Get URL Parts
$urls = explode("/", $_SERVER['REQUEST_URI']);
array_shift($urls);

// Get Data from Files
$details = decodeJsonFile("json/details.json", true);
$text = decodeJsonFile("json/language.json", true);
$lang = getLanguage();

?>

<!DOCTYPE html>
<html class="no-js">
<head>
    <title><?php echo $details['Name']; ?></title>
    <link rel="stylesheet" href="/css/styles.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <?php

    if($urls[0] == ""){

    ?>

    <div class="landing-page">
        <div class="company-details">
            <h1 class="company-name"><?php echo $details['Name']; ?></h1>
            <span class="company-job"><?php echo $text[$lang][0]; ?></span>
        </div>
        <div class="languages">
            <?php
                // Display languages except for current one
                echo "<ul>";
                foreach (array_keys($text) as $value) {
                    if($value != $lang) echo "<li><a href='/".$value."'>".strtoupper($value)."</a></li>";
                }
                echo "</ul>";

                // Display current language
                echo strtoupper($lang);

            ?>
        </div>
    </div>
    <div class="text-container">
        <p><?php echo $text[$lang][1]; ?></p>
        <p><?php echo $text[$lang][2]; ?></p>
    </div>
    <div class="stone-preview">
        <div class="building">
            <h2><?php echo $text[$lang][3]; ?></h2>
            <picture>
                <source srcset="/img/buildingstone01.webp" type="image/webp">
                <source srcset="/img/buildingstone01.jpg" type="image/jpeg">
                <img src="/img/buildingstone01.jpg" alt="<?php echo $text[$lang][3]; ?> 01">
            </picture>
            <picture>
                <source srcset="/img/buildingstone02.webp" type="image/webp">
                <source srcset="/img/buildingstone02.jpg" type="image/jpeg">
                <img src="/img/buildingstone02.jpg" alt="<?php echo $text[$lang][3]; ?> 02">
            </picture>
            <picture>
                <source srcset="/img/buildingstone03.webp" type="image/webp">
                <source srcset="/img/buildingstone03.jpg" type="image/jpeg">
                <img src="/img/buildingstone03.jpg" alt="<?php echo $text[$lang][3]; ?> 03">
            </picture>
        </div>
        <div class="granite">
            <h2><?php echo $text[$lang][4]; ?></h2>
            <picture>
                <source srcset="/img/granite01.webp" type="image/webp">
                <source srcset="/img/granite01.jpg" type="image/jpeg">
                <img src="/img/granite01.jpg" alt="<?php echo $text[$lang][4]; ?> 01">
            </picture>
            <picture>
                <source srcset="/img/granite02.webp" type="image/webp">
                <source srcset="/img/granite02.jpg" type="image/jpeg">
                <img src="/img/granite02.jpg" alt="<?php echo $text[$lang][4]; ?> 02">
            </picture>
            <picture>
                <source srcset="/img/granite03.webp" type="image/webp">
                <source srcset="/img/granite03.jpg" type="image/jpeg">
                <img src="/img/granite03.jpg" alt="<?php echo $text[$lang][4]; ?> 03">
            </picture>
        </div>
        <div class="special">
            <h2><?php echo $text[$lang][5]; ?></h2>
            <picture>
                <source srcset="/img/special01.webp" type="image/webp">
                <source srcset="/img/special01.jpg" type="image/jpeg">
                <img src="/img/special01.jpg" alt="<?php echo $text[$lang][5]; ?> 01">
            </picture>
            <picture>
                <source srcset="/img/special02.webp" type="image/webp">
                <source srcset="/img/special02.jpg" type="image/jpeg">
                <img src="/img/special02.jpg" alt="<?php echo $text[$lang][5]; ?> 02">
            </picture>
            <picture>
                <source srcset="/img/special03.webp" type="image/webp">
                <source srcset="/img/special03.jpg" type="image/jpeg">
                <img src="/img/special03.jpg" alt="<?php echo $text[$lang][5]; ?> 03">
            </picture>
        </div>
    </div>
    <div class="text-container">
        <?php
            echo $text[$lang][6]."<br />";
            echo "<span class='contact'>".$text[$lang][8]."</span>";
        ?>
    </div>
    <div class="contactWrapper">
        <div class="contactForm" data-lang="<?php echo $lang; ?>">
            <span class="contactClose"><i class="fa fa-times"></i></span>
            <div class="contactDetails">
                <div class="bold"><?php echo $details["Name"]; ?></div>
                <div class="contactPerson">
                    <i class="fa fa-user-tie"></i> <?php echo $details["Owner"]; ?>
                </div>
                <div class="contactPhone"><a href="tel:<?php echo $details["Telefon"]; ?>">
                    <i class="fa fa-phone"></i> <?php echo $details["Telefon"]; ?>
                </a></div>
                <div class="contactAddress"><a href="http://maps.google.com/?q=<?php echo $details["Address"]; ?>">
                    <i class="fa fa-map-marker-alt"></i> <?php echo $details["Address"]; ?>
                </a></div>
            </div>

            <div class="bold">Email</div>
            <form method="post" onsubmit="return handleFormSubmit();">
                <input required type="text" name="name" placeholder="<?php echo $text[$lang][9] ?>" /><br />
                <input required type="email" name="email" placeholder="<?php echo $text[$lang][10] ?>" /><br />
                <textarea required name="msg" placeholder="<?php echo $text[$lang][11] ?>"></textarea>
                <input type="submit" value="<?php echo $text[$lang][12] ?>" />
                <span class="formMessage"></span>
            </form>
        </div>
    </div>
<?php
    }else{
        require("error404.php");
    }
?>

    <script src="/js/scripts.min.js"></script>
</body>
</html>

<?php

/*********
FUNCTIONS
**********/

// Get Language for all Texts
function getLanguage(){
    $langs = array("en", "de", "cn", "it");
    $firstUrl = $GLOBALS['urls'][0];

    // First Urls parts is a language
    if(in_array($firstUrl, $langs)){
        return array_shift($GLOBALS['urls']);
    }

    // If no language specified, use english
    return "en";
}

// Read File and then decode json
function decodeJsonFile($file, $assoc){
    return json_decode(file_get_contents("../".$file), $assoc);
}

?>
