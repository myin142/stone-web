var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var pump = require('pump');

var config = {
    assets: 'assets/',
    sassPattern: '/sass/**/*.scss',
    jsPattern: '/js/**/*.js'
}

gulp.task('sass', function(cb){
    pump([
        gulp.src(config.assets + config.sassPattern),
        sass(),
        concat('styles.min.css'),
        cleanCSS(),
        gulp.dest('public/css')
    ], cb);
});

gulp.task('uglify', function(cb){
    pump([
        gulp.src(config.assets + config.jsPattern),
        uglify(),
        concat('scripts.min.js'),
        gulp.dest('public/js')
    ], cb);
});

gulp.task('watch', function(){
    gulp.watch(config.assets + config.sassPattern, ['sass']);
    gulp.watch(config.assets + config.jsPattern, ['uglify']);
});

gulp.task('default', ['sass', 'uglify', 'watch']);
